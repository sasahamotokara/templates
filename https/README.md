# HTTPS対応Docker開発環境セットアップスクリプト

Docker環境において簡単にHTTPS通信を導入できるセットアップスクリプトです。

## 前提条件
- Node.js v20 以降がインストールされていること
- Docker および docker-compose が利用可能であること

## セットアップ手順

1. **`docker-compose.yml` に nginx サービスを追加**
   以下の例を参考に、HTTPS通信を有効化するサービス名を `depends_on` に指定します。

   ```yaml
   services:
     nginx:
       image: nginx:latest
       ports:
         - "443:443"
         - "80:80"
       volumes:
         - ./nginx/conf.d:/etc/nginx/conf.d
         - ./nginx/certs:/etc/ssl/certs
       depends_on:
         - web
         - api
   ```

2. **依存関係のインストール**
   プロジェクトルートで以下のコマンドを実行します。

   ```bash
   # npm を使用している場合
   npm install

   # yarn を使用している場合
   yarn install

   # pnpm を使用している場合
   pnpm install
   ```

3. **必要に応じて `scripts/setup.js` をカスタマイズ**
   設定を変更する場合は、以下のポイントを参考にしてください。

   ### カスタマイズの例
   - **Nginxサービス名の変更**: `nginxServiceName` を書き換える
   - **サービス名とドメインの対応付け**: `domains` に設定する
       - 以下のドメインの使用を推奨します。これらは予約済みドメインのため、 **インターネット上の実在ドメインと衝突しません。**
           - `.localhost`
           - `.test`
           - `.example`（`.example.com`, `.example.net`, `.example.org`）
           - `.invalid`
   - **入出力パスの変更**: `paths` を修正する
   - **証明書ファイル名の変更**: `certificates` を修正する

   ```javascript
   /**
    * nginxServiceName - Nginxサービス名
    * @constant
    * @type {string}
    */
   const nginxServiceName = 'nginx';

   /**
    * domains - ドメインのリスト
    * @constant
    * @type {{[key: string]: string}}
    * @description Dockerのサービス名と一致するキー名を持つ必要があります
    */
   const domains = {
       web: 'sample.localhost',
       api: 'sample-api.localhost'
   };

   /**
    * paths - ディレクトリのパス
    * @constant
    * @type {object}
    * @property {string} nginx - Nginxディレクトリのパス
    * @property {string} bin - バイナリディレクトリのパス
    * @property {string} certs - 証明書ディレクトリのパス
    * @property {string} conf - Nginx設定ファイルのディレクトリのパス
    * @property {string} docker - Docker Composeファイルのパス
    */
   const paths = {
       nginx: fileURLToPath(new URL('../nginx', import.meta.url)),
       bin: fileURLToPath(new URL('../nginx/bin', import.meta.url)),
       certs: fileURLToPath(new URL('../nginx/certs', import.meta.url)),
       conf: fileURLToPath(new URL('../nginx/conf.d', import.meta.url)),
       docker: fileURLToPath(new URL('../docker-compose.yml', import.meta.url)),
   };

   /**
    * certificates - 証明書ファイル名
    * @constant
    * @type {object}
    * @property {string} crt - 証明書ファイル名
    * @property {string} key - 秘密鍵ファイル名
    */
   const certificates = {
       crt: 'localhost.crt',
       key: 'localhost.key'
   };
   ```

4. **セットアップスクリプトの実行**
   ```bash
   node scripts/setup.js
   ```

5. **Dockerコンテナの再構築**
   ```bash
   docker compose build
   docker compose up -d
   ```

## 諸注意
- crt/keyファイルをチームで共有して使用する場合、信頼されていない証明書を共有することになるためご注意ください。
    - そのため、各開発者がこのスクリプトを使用して証明書を発行する手順を推奨します。
    - `.gitignore`を使用して、`nginx`ディレクトリ（またはcrt/keyファイル）を追跡しないようにしてください。
- FireFoxでは、独自の証明書ストアが使用されているため、「潜在的なセキュリティリスクあり」と表示されることがあります。  
この場合、以下の手順を参照してFireFoxに証明書をインポートすることで回避できます。
    1. Firefoxのメニュー（≡）→「設定」を開く
    2. 左側のメニューから「プライバシーとセキュリティ」を選択
    3. 下にスクロールして「証明書」セクションを探す
    4. 「証明書を表示」をクリック
    5. 「認証局証明書」のタブで「インポート」をクリック
    6. `mkcert` によって生成されたルートCAを選択
        - 通常は以下の場所にあります:
            - Linux/macOS: `~/.local/share/mkcert/rootCA.pem`
            - Windows: `C:\Users\<ユーザー名>\AppData\Local\mkcert\rootCA.pem`
    7. インポート時、「このCAを信頼する」にチェックを入れる（特に「Webサイトの識別」用）
