import fs from 'node:fs';
import path from 'node:path';
import {execFile} from 'node:child_process';
import {fileURLToPath, URL} from 'node:url';
import {pipeline} from 'node:stream/promises';
import {Readable} from 'node:stream';
import YAML from 'yaml';

const {platform, arch} = process;

/**
 * nginxServiceName - Nginxサービス名
 * @constant
 * @type {string}
 */
const nginxServiceName = 'nginx';

/**
 * domains - ドメインのリスト
 * @constant
 * @type {{[key: string]: string}}
 * @description Dockerのサービス名と一致するキー名を持つ必要があります
 */
const domains = {
    web: 'sample.localhost',
    api: 'sample-api.localhost'
};

/**
 * paths - ディレクトリのパス
 * @constant
 * @type {object}
 * @property {string} nginx - Nginxディレクトリのパス
 * @property {string} bin - バイナリディレクトリのパス
 * @property {string} certs - 証明書ディレクトリのパス
 * @property {string} conf - Nginx設定ファイルのディレクトリのパス
 * @property {string} docker - Docker Composeファイルのパス
 */
const paths = {
    nginx: fileURLToPath(new URL('../nginx', import.meta.url)),
    bin: fileURLToPath(new URL('../nginx/bin', import.meta.url)),
    certs: fileURLToPath(new URL('../nginx/certs', import.meta.url)),
    conf: fileURLToPath(new URL('../nginx/conf.d', import.meta.url)),
    docker: fileURLToPath(new URL('../docker-compose.yml', import.meta.url)),
};

/**
 * certificates - 証明書ファイル名
 * @constant
 * @type {object}
 * @property {string} crt - 証明書ファイル名
 * @property {string} key - 秘密鍵ファイル名
 */
const certificates = {
    crt: 'localhost.crt',
    key: 'localhost.key'
};

/**
 * getMkcertMetadata - mkcertのメタデータを取得
 * @returns {Promise<object>} mkcertのメタデータ
 */
const getMkcertMetadata = async () => {
    try {
        // GitHub APIから最新のmkcertリリース情報を取得
        const response = await fetch('https://api.github.com/repos/FiloSottile/mkcert/releases/latest');
        const json = await response.json();

        return json;
    } catch (error) {
        console.error(`[failed]: Failed to fetch metadata from GitHub API: ${error.message}`);
    }
};

/**
 * getPlatform - プラットフォーム情報を取得
 * @returns {string} プラットフォーム情報
 * @throws {Error} サポートされていないアーキテクチャまたはプラットフォームの場合
 */
const getPlatform = () => {
    const isARM = arch.startsWith('arm');
    const PlatForm = platform === 'win32' ? 'windows' : platform;
    const Architecture = isARM ? arch : 'amd64';

    // サポートされていないアーキテクチャの場合にエラーをスロー
    if (arch !== 'x64' && !isARM) {
        throw new Error(`[failed]: Only x64/arm/arm64 architectures are supported at this time: ${arch}`);
    }

    // サポートされていないプラットフォームの場合にエラーをスロー
    if (!['windows', 'darwin', 'linux'].includes(PlatForm)) {
        throw new Error(`[failed]: Unsupported platform: ${PlatForm}`);
    }

    return `${PlatForm}-${Architecture}`;
};

/**
 * getMkcertUrl - mkcertのダウンロードURLを取得
 * @returns {Promise<string>} mkcertのダウンロードURL
 */
const getMkcertUrl = async () => {
    // mkcertのメタデータを取得
    const metadata = await getMkcertMetadata();
    const platform = getPlatform();
    const assets = metadata.assets.map(({browser_download_url: url}) => url);

    // プラットフォームに対応するURLを検索
    return assets.find((url) => url.includes(platform));
};

/**
 * getMkcertLocalPath - mkcertのローカルパスを取得
 * @returns {string} mkcertのローカルパス
 */
const getMkcertLocalPath = () => {
    const {bin} = paths;

    // バイナリディレクトリが存在しない場合は作成
    if (!fs.existsSync(bin)) {
        fs.mkdirSync(bin);
    }

    return path.join(bin, platform === 'win32' ? 'mkcert.exe' : 'mkcert');
};

/**
 * downloadFile - ファイルをダウンロード
 * @param {string} url - ダウンロードURL
 * @param {string} dest - 保存先パス
 * @returns {Promise<void>}
 * @throws {Error} ダウンロードに失敗した場合
 */
const downloadFile = async (url, dest) => {
    // ファイルをダウンロード
    const response = await fetch(url);

    // ダウンロードが成功したか確認
    if (!response.ok) {
        throw new Error(`[failed]: Failed to download file: ${url} (status: ${response.status})`);
    }

    const nodeStream = Readable.fromWeb(response.body);
    const fileStream = fs.createWriteStream(dest);

    // ダウンロードしたデータをファイルに保存
    await pipeline(nodeStream, fileStream);
};

/**
 * execPromise - コマンドを実行
 * @param {string} command - コマンド
 * @param {string[]} args - 引数
 * @param {object} [options={}] - オプション
 * @returns {Promise<object>} 実行結果
 */
const execPromise = (command, args, options = {}) => new Promise((resolve, reject) => {
    execFile(command, args, options, (error, stdout, stderr) => {
        if (error) {
            reject({error, stdout, stderr});
        } else {
            resolve({stdout, stderr});
        }
    });
});

/**
 * ensureMkcert - mkcertが存在するか確認し、存在しない場合はダウンロード
 * @returns {Promise<string>} mkcertのパス
 */
const ensureMkcert = async () => {
    const mkcertPath = getMkcertLocalPath();

    // mkcertが存在しない場合はダウンロード
    if (!fs.existsSync(mkcertPath)) {
        console.log('[info]: Mkcert not found. Starting download.');

        await downloadFile(await getMkcertUrl(), mkcertPath);

        // Windows以外のプラットフォームでは実行権限を付与
        if (platform !== 'win32') {
            fs.chmodSync(mkcertPath, 0o755);
        }

        console.log('[success]: Download completed.');
    } else {
        console.log('[skip]: Using local mkcert.');
    }

    return mkcertPath;
};

/**
 * generateNginxConf - Nginx設定ファイルを生成
 * @returns {Promise<void>}
 * @throws {Error} Nginxサービスが見つからない場合、または依存関係がない場合
 */
const generateNginxConf = async () => {
    const {docker, conf} = paths;
    const dockerCompose = fs.readFileSync(docker, 'utf8');
    const {services = {}} = YAML.parse(dockerCompose);
    let defaultConf = '';

    console.log('[info]: Generating nginx conf...');

    // Nginxサービスが存在するか確認
    if (!Object.hasOwn(services, nginxServiceName)) {
        throw new Error('[failed]: Nginx service not found in docker-compose.yml');
    }

    // Nginxサービスに依存関係があるか確認
    if (!Object.hasOwn(services[nginxServiceName], 'depends_on')) {
        throw new Error('[failed]: Nginx service has no dependencies in docker-compose.yml');
    }

    // Nginx設定ファイルのディレクトリが存在しない場合は作成
    if (!fs.existsSync(conf)) {
        fs.mkdirSync(conf);
    }

    defaultConf += services[nginxServiceName].depends_on.map((service) => {
        const [, port] = services[service].ports[0].split(':');
        const domain = domains[service];

        return `server {
    listen 443 ssl;
    server_name ${domain};

    ssl_certificate     /etc/ssl/certs/${certificates.crt};
    ssl_certificate_key /etc/ssl/certs/${certificates.key};

    location / {
        proxy_pass http://${service}:${port};
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
    }
}
`;
    }).join('\n');

    defaultConf += `
server {
    listen 80;
    server_name ${Object.values(domains).join(' ')};
    return 301 https://$host$request_uri;
}`

    try {
        fs.writeFileSync(path.join(conf, 'default.conf'), defaultConf, 'utf8');
        console.log('[success]: nginx configuration generated.');
    } catch (error) {
        console.error('[failed]: Failed to generate nginx configuration:', error);
    }
};

(async() => {
    const {nginx, certs} = paths;
    const mkcertPath = await ensureMkcert();
    const domainNames = Object.values(domains);
    const certFile = path.join(certs, certificates.crt);
    const keyFile = path.join(certs, certificates.key);

    try {
        // Nginxディレクトリが存在しない場合は作成
        if (!fs.existsSync(nginx)) {
            fs.mkdirSync(nginx);
        }

        // 証明書ディレクトリが存在しない場合は作成
        if (!fs.existsSync(certs)) {
            fs.mkdirSync(certs);
        }

        console.log('[info]: Installing local CA with mkcert...');

        // mkcertでローカルCAをインストール
        await execPromise(mkcertPath, ['-install']);

        console.log('[success]: Local CA installation completed.');

        console.log(`[info]: Generating certificates for ${domainNames.join(' and ')}...`);

        // mkcertで証明書を生成
        await execPromise(mkcertPath, [
            '-cert-file',
            certFile,
            '-key-file',
            keyFile,
            ...domainNames
        ]);

        console.log('[success]: Certificates generated.');

        // Nginx設定ファイルを生成
        await generateNginxConf();

        console.log(`
INFORMATION: Next steps...
    Please run: \`docker compose build\`
    Then run: \`docker compose up -d\`

You can access the services at:
${Object.values(domains).map((domain) => `    - https://${domain}`).join('\n')}
`);
    } catch (error) {
        console.error('[failed]: An error occurred during setup:', error);
    }
})();
