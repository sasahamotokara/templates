export default {
    extends: ['stylelint-config-standard', 'stylelint-config-recess-order'],
    overrides: [
        {
            files: ['*.scss', '**/*.scss'],
            extends: [
                'stylelint-config-standard-scss',
                'stylelint-config-recommended-scss',
                'stylelint-config-recess-order'
            ]
        },
        {
            files: ['*.vue', '**/*.vue'],
            extends: [
                'stylelint-config-standard-scss',
                'stylelint-config-recommended-scss',
                'stylelint-config-recommended-vue',
                'stylelint-config-recess-order'
            ]
        }
    ],
    ignoreFiles: ['node_modules/**', 'web/storybook-static/**', 'web/dist/**'],
    ignoreProperties: ['fill'],
    rules: {
        'at-rule-no-unknown': null,
        'at-rule-empty-line-before': ['always', {ignore: ['after-comment']}],
        'rule-empty-line-before': [
            'always-multi-line',
            {ignore: ['after-comment']}
        ],
        'no-descending-specificity': null,
        'comment-empty-line-before': null,
        'selector-class-pattern':
            '^[a-z]([a-z0-9-]+)?(__([a-z0-9]+-?)+)?(--([a-z0-9]+-?)+){0,2}$',
        'function-no-unknown': [
            true,
            {ignoreFunctions: ['/math\\..+/', 'v-bind']}
        ],
        'selector-pseudo-element-no-unknown': [
            true,
            {ignorePseudoElements: ['v-deep']}
        ]
    }
};
