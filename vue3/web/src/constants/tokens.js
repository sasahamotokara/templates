/**
 * [Warning]:
 * Don't update this file manually.
 * This is auto generated token variables from "Style Dictionary".
 * Please update "web/.style-dictionary/tokens/*.yaml" and run `pnpm run styled`.
 */

/**
 * colorSchema
 * @type {object}
 */
export const colorSchema = {
    'gray-0': '#fff',
    'gray-5': '#f1f1f1',
    'gray-15': '#d9d9d9',
    'gray-50': '#999',
    'gray-70': '#4d4d4d',
    'gray-90': '#333',
    'primary-10': '#fceee8',
    'primary-30': '#f8cabc',
    'primary-100': '#c00033',
    'text-strong': '#333',
    'text-medium': '#4d4d4d',
    'text-low': '#d9d9d9',
    'text-white': '#fff'
};

/**
 * effect
 * @type {object}
 */
export const effect = {
    'focus-ling': '2px solid #9ccbf3',
    'shadow-lv1': '0 3px 6px 1px rgba(#000, .16)',
    'shadow-lv2': '0 5px 10px 2px rgba(#000, .18)',
    'shadow-lv3': '0 8px 12px 4px rgba(#000, .2)',
    'shadow-lv4': '0 12px 20px 4px rgba(#000, .2)'
};

/**
 * fontWeight
 * @type {object}
 */
export const fontWeight = {medium: '500', bold: '700', black: '900'};

/**
 * breakpoints
 * @type {object}
 */
export const breakpoints = {pc: '1024px <= width', tb: 'width <= 1023px', sp: 'width <= 767px'};

/**
 * typeScale
 * @type {object}
 */
export const typeScale = {small: '10rem', medium: '16rem', large: '24rem'};

export default {
    colorSchema,
    effect,
    fontWeight,
    breakpoints,
    typeScale
};
