import {fileURLToPath, URL} from 'node:url';

/** @type { import('@storybook/vue3-vite').StorybookConfig } */
const configure = {
    stories: ['../src/**/*.mdx', '../src/**/*.stories.@(js|jsx|mjs|ts|tsx)'],
    addons: [
        '@storybook/addon-links',
        '@storybook/addon-essentials',
        '@chromatic-com/storybook',
        '@storybook/addon-interactions',
        '@storybook/addon-docs'
    ],
    framework: {
        name: '@storybook/vue3-vite',
        options: {}
    },
    docs: {
        autodocs: 'tag'
    },
    async viteFinal(cnf, {configType}) {
        const config = {...cnf};
        const {
            plugins,
            resolve: {alias},
            css
        } = await import('../../vite.config.js').then(({default: viteConfig}) => viteConfig());

        if (configType === 'DEVELOPMENT') {
            config.server.watch = {usePolling: true};
        }

        config.cacheDir = fileURLToPath(new URL('../../node_modules/.vite-storybook', import.meta.url));
        config.plugins = config.plugins.concat(plugins);
        config.resolve.alias = Object.assign(config.resolve.alias, alias);
        config.css = css;

        return config;
    }
};

export default configure;
