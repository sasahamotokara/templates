/** @type { import('@storybook/vue3').Preview } */
const preview = {
    parameters: {
        backgrounds: {
            default: 'white',
            values: [
                {
                    name: 'white',
                    value: '#fff'
                }
            ]
        },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/
            }
        }
    }
};

export default preview;
