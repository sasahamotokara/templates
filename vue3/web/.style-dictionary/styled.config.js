import StyleDictionary from 'style-dictionary';
import yaml from 'yaml';

/**
 * comments - 定数ファイル内のコメント指定
 * @type {object}
 * @property {string} warning            - 警告メッセージ
 * @property {function} js.defaultExport - JavaScriptデフォルトエクスポートコメント
 * @property {function} js.export        - JavaScriptエクスポートコメント
 * @property {function} css.header       - CSSセクションヘッダコメント
 * @property {function} css.sub          - CSSサブセクションコメント
 */
const comments = {
    warning: `/**
 * [Warning]:
 * Don't update this file manually.
 * This is auto generated token variables from "Style Dictionary".
 * Please update "web/.style-dictionary/tokens/*.yaml" and run \`pnpm run styled\`.
 */`,
    js: {
        defaultExport: (variables) => `export default {${Object.keys(variables).join(', ')}};`,
        export: (variable, value) => `
/**
 * ${variable}
 * @type {${typeof value}}
 */
export const ${variable} = ${JSON.stringify(value)};`
    },
    css: {
        header: (name) => `/* ----------------------------------------------------------------
 *    ${name}
 * ---------------------------------------------------------------- */`,
        sub: (name) => `/* ${name}
----------------------------------------------- */`
    }
};

/**
 * JavaScriptの定数ファイルのカスタムフォーマット
 */
StyleDictionary.registerFormat({
    name: 'javascript/esm',
    format: async ({allTokens}) => {
        // トークンをフォーマットする
        const formattedStyles = allTokens.reduce(
            (object, {attributes, filePath, name, value, format = ''}) => {
                const variableName = /\/?([^\/]+)\.yaml$/.exec(filePath).at(-1);

                // map-flat型の場合、Object型で出力
                if (format === 'map-flat') {
                    object[name] ??= {};
                    Object.assign(object, {
                        [name]: Object.fromEntries(
                            Object.entries(value).map(([key, {value: val}]) => [key, val])
                        )
                    });

                    return object;
                }

                object[variableName] ??= {};
                Object.assign(object[variableName], {[name.replace(`${attributes.category}-`, '')]: value});

                return object;
            },
            {}
        );

        return `${comments.warning}
${Object.entries(formattedStyles)
    .map(([key, value]) => comments.js.export(key, value))
    .join('\n')}

${comments.js.defaultExport(formattedStyles)}
`;
    }
});

/**
 * SASSの定数ファイルのカスタムフォーマット
 */
StyleDictionary.registerFormat({
    name: 'scss/variables-with-comment',
    format: async ({allTokens, tokens}) => {
        const formattedStyles = allTokens.reduce(
            (object, {attributes: {category, type}, name, value}, index, self) => {
                const previous = self.at(index - 1);
                const {description = ''} = tokens[category][type] || {};

                object[category] ??= '';

                // 処理の前後でtypeが変動する場合は、サブセクションコメントを追加
                if (description && previous.attributes.type !== type) {
                    object[category] += `${!object[category] ? '' : '\n'}\n${comments.css.sub(description)}`;
                }

                object[category] += `\n$${name}: ${value};`;

                return object;
            },
            {}
        );

        // SASS変数文字列を生成
        const scssString = Object.entries(formattedStyles)
            .map(
                ([category, variables]) => `
${comments.css.header(category)}${variables}`
            )
            .join('\n');

        return `${comments.warning}\n${scssString}\n`;
    }
});

/**
 * SASSのmap-flat型への変換処理を定義
 */
StyleDictionary.registerTransform({
    type: 'value',
    name: 'map-flat',
    filter: ({format = ''}) => format === 'map-flat',
    transform: ({value}) => `(
    ${Object.entries(value)
        .map(([key, query]) => `'${key}': '${query.value}'`)
        .join(',\n    ')}
) !default`
});

/**
 * .yamlファイルの読み込み・パースを有効化
 */
StyleDictionary.registerParser({
    name: 'yaml-perser',
    pattern: /\.yaml$/,
    parser: ({contents}) => yaml.parse(contents)
});

export default {
    parsers: ['yaml-perser'],
    source: ['web/.style-dictionary/tokens/**/*.yaml'],
    platforms: {
        scss: {
            transforms: [...StyleDictionary.hooks.transformGroups.scss, 'map-flat'],
            buildPath: 'web/src/styles/',
            files: [
                {
                    format: 'scss/variables-with-comment',
                    destination: '_variables.scss'
                }
            ]
        },
        js: {
            transforms: ['attribute/cti', 'name/kebab', 'size/rem', 'color/hex'],
            buildPath: 'web/src/constants/',
            files: [
                {
                    format: 'javascript/esm',
                    destination: 'tokens.js'
                }
            ]
        }
    }
};
