import pluginVue from 'eslint-plugin-vue';
import pluginA11yVue from 'eslint-plugin-vuejs-accessibility';
import {FlatCompat} from '@eslint/eslintrc';
import js from '@eslint/js';
import jsdoc from 'eslint-plugin-jsdoc';
import prettier from '@vue/eslint-config-prettier';

// ESLint v9 に対応していないルールを `extends` で読み込むための設定
const compat = new FlatCompat().extends(
    'plugin:storybook/recommended',
    '@vue/eslint-config-airbnb'
);

/**
 * @type {Array<String>} - 除外するルールのリスト
 * @description eslint-config-airbnb内の廃止されたルールを削除するためのリスト
 */
const ignoreRules = [
    'react/jsx-no-bind',
    'import/no-mutable-exports',
    'import/no-amd',
    'import/no-named-as-default',
    'react/no-array-index-key',
    'import/newline-after-import',
    'import/no-named-as-default-member'
];

// ESLint設定から除外ルールを削除
compat.forEach(({rules = {}}) => {
    const keys = Object.keys(rules);

    ignoreRules.forEach((name) => {
        if (!keys.includes(name)) {
            return;
        }

        delete rules[name];
    });
});

// vue/no-template-shadow を無効化
pluginVue.configs['flat/strongly-recommended'].at(-1).rules['vue/no-template-shadow'] = 'off';

/**
 * ESLint設定をエクスポート
 * @type {Array} - ESLint設定の配列
 */
export default [
    js.configs.recommended,
    jsdoc.configs['flat/recommended'],
    ...pluginVue.configs['flat/essential'],
    ...pluginVue.configs['flat/strongly-recommended'],
    ...pluginA11yVue.configs['flat/recommended'],
    ...compat,
    prettier,
    {
        files: ['./web/**/*.{vue,js,jsx,cjs,mjs}'],
        languageOptions: {
            sourceType: 'module',
            globals: {},
            parserOptions: {ecmaVersion: 'latest'}
        },
        linterOptions: {},
        plugins: {jsdoc},
        settings: {
            'import/resolver': {
                vite: {
                    configPath: './vite.config.js',
                    extensions: ['.vue', '.js', '.jsx', '.cjs', '.mjs']
                },
                alias: {
                    map: [['@', './src']],
                    extensions: ['.vue', '.js', '.jsx', '.cjs', '.mjs']
                }
            }
        },
        rules: {
            'react/jsx-no-bind': 'off',
            'vue/html-indent': ['error', 4],
            'vue/no-multiple-template-root': 0,
            'vue/multi-word-component-names': 0,
            'vue/object-curly-spacing': 0,
            'vue/comma-dangle': 0,
            'vue/max-len': 0,
            'vuejs-accessibility/form-control-has-label': 0,
            'vuejs-accessibility/label-has-for': 0,
            'no-param-reassign': [
                'error',
                {
                    props: false
                }
            ],
            'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
            'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
            'linebreak-style': 0,
            indent: ['error', 4],
            semi: [2, 'always'],
            'object-curly-spacing': [2, 'never'],
            'space-before-function-paren': [
                2,
                {
                    anonymous: 'always',
                    named: 'never',
                    asyncArrow: 'always'
                }
            ],
            'max-len': 0,
            'comma-dangle': [2, 'never'],
            yoda: 0,
            'no-multiple-empty-lines': [
                2,
                {
                    max: 3
                }
            ],
            'jsdoc/require-description': 'warn'
        }
    },
    /**
     * ignore（除外）設定はオブジェクトを切り分けないと動作しない
     * @see {@link https://github.com/eslint/eslint/issues/17400}
     */
    {
        ignores: ['**/*.config.{js,cjs,mjs}', 'node_modules/**', 'web/dist/**', 'web/storybook-static/**']
    }
];
