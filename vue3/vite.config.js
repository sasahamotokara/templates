import {fileURLToPath, URL} from 'node:url';
import {defineConfig} from 'vite';
import vue from '@vitejs/plugin-vue';
import eslintPlugin from '@nabla/vite-plugin-eslint';
import terser from '@rollup/plugin-terser';
import stylelintPlugin from 'vite-plugin-stylelint';
import autoprefixer from 'autoprefixer';

export default defineConfig(() => ({
    root: fileURLToPath(new URL('./web', import.meta.url)),
    publicDir: fileURLToPath(new URL('./web/public', import.meta.url)),
    plugins: [
        vue(),
        eslintPlugin({
            eslintPath: 'eslint/use-at-your-own-risk'
        }),
        stylelintPlugin(),
        terser({
            compress: {
                drop_console: true
            }
        })
    ],
    server: {
        host: true,
        port: 8080,
        watch: {
            usePolling: true
        }
    },
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    css: {
        preprocessorMaxWorkers: true,
        preprocessorOptions: {
            scss: {
                additionalData: '@use "sass:math";'
            }
        },
        postcss: {
            plugins: [autoprefixer()]
        }
    }
}));
