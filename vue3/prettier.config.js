const config = {
    printWidth: 110,
    tabWidth: 4,
    useTabs: false,
    semi: true,
    singleQuote: true,
    trailingComma: 'none',
    bracketSpacing: false,
    arrowParens: 'always',
    requirePragma: false,
    insertPragma: false,
    proseWrap: 'never',
    htmlWhitespaceSensitivity: 'ignore'
};

export default config;
