export default {
    extends: ['markuplint:recommended'],
    parser: {
        '.vue$': '@markuplint/vue-parser'
    },
    specs: {
        '.vue$': '@markuplint/vue-spec'
    },
    excludeFiles: [],
    pretenders: [],
    nodeRules: [
        {
            selector: 'svg',
            rules: {
                'require-accessible-name': false
            }
        },
        {
            selector: 'path',
            rules: {
                'require-accessible-name': false
            }
        }
    ],
    rules: {
        'invalid-attr': {
            options: {
                ignoreAttrNamePrefix: ['@']
            }
        }
    }
};
