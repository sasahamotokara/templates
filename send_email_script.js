import {readFileSync} from 'node:fs';
import nodemailer from 'nodemailer';
import gems from './gem_updates.json' with {type: 'json'};

const {EMAIL_ADDRESS, EMAIL_PASSWORD} = process.env;
const mailList = Object.entries(process.env).reduce((array, [key, value]) => {
    if (key.startsWith('EMAIL_SEND_TO')) {
        array.push(value);
    }

    return array;
}, []);

// updates.txtの内容を読み込む
const nodeUpdates = readFileSync('node_updates.txt', 'utf-8');
const rubyUpdates = readFileSync('ruby_updates.txt', 'utf-8');

const maxColumnWidths = gems.reduce((widths, {gem, current, latest}) => {
    widths.gem = Math.max((widths.gem || 0), gem.length);
    widths.current = Math.max((widths.current || 0), current.length);
    widths.latest = Math.max((widths.latest || 0), latest.length);

    return widths;
}, {});

const gemUpdates = gems.map(({gem, current, latest}) => `${gem.padEnd(maxColumnWidths.gem)}\t\t${current.padEnd(maxColumnWidths.current)}\t→\t${latest.padEnd(maxColumnWidths.latest)}`).join('\n');

// トランスポーター
const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: EMAIL_ADDRESS,
        pass: EMAIL_PASSWORD
    }
});

// メールオプション
const mailOptions = {
    from: EMAIL_ADDRESS,
    subject: 'Daily Check Updates Report.',
    text: `The following npm packages have updates available:
${nodeUpdates}
----

${rubyUpdates}

The following gem packages have updates available:
${gemUpdates}`
};

mailList.forEach((to) => {
    // メールの送信
    transporter.sendMail({...mailOptions, to}, (error, info) => {
        if (error) {
            console.error('Error sending email:', error);

            return;
        }

        console.log('Email sent:', info.response);
    });
});
